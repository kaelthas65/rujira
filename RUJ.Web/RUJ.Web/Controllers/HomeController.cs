﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using BusinessLayer.Repositories.ProjectTasks;
using BusinessLayer.Repositories.Users;
using DataLayer.Entities;
using DataLayer.Entities.Enums;
using Microsoft.AspNetCore.Mvc;
using RUJ.Web.Models;

namespace RUJ.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly IProjectTaskRepository projectTaskRepository;

        public HomeController(IUserRepository userRepository, IProjectTaskRepository projectTaskRepository)
        {
            this.userRepository = userRepository;
            this.projectTaskRepository = projectTaskRepository;
        }

        public IActionResult Users()
        {
            var users = userRepository.GetAll().ToList();
            return View(users);
        }

        public IActionResult Tasks()
        {
            if (User.Identity.IsAuthenticated)
            {
                List<ProjectTask> tasks;
                var userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value);

                if (User.IsInRole("Director"))
                {
                    tasks = projectTaskRepository.GetAll().ToList();
                }
                else
                {
                    tasks = projectTaskRepository.GetByUserId(userId).ToList();
                }

                return View(tasks);
            }

            return new EmptyResult();
        }

        public IActionResult UserView(int userId)
        {
            var user = userRepository.GetById(userId);
            return View(user);
        }

        [HttpGet]
        public IActionResult UserEdit(int userId)
        {
            User user = new User();

            if (userId != 0)
            {
                user = userRepository.GetById(userId);
            }

            return View(user);
        }

        [HttpPost]
        public IActionResult TaskEdit(ProjectTask task)
        {
            projectTaskRepository.Save(task);

            return RedirectToAction("TaskView", "Home", new { taskId = task.Id });
        }

        [HttpPost]
        public IActionResult TaskChangeState(ProjectTaskStateEnum.ProjectTaskState projectTaskState, int taskId)
        {
            var task = projectTaskRepository.GetById(taskId);
            task.ProjectTaskState = projectTaskState;
            projectTaskRepository.Save(task);

            return RedirectToAction("TaskView", "Home", new { taskId = task.Id });
        }

        [HttpPost]
        public IActionResult TaskSpendTime(int timeSpend, int taskId)
        {
            var task = projectTaskRepository.GetById(taskId);
            task.SpentTime += timeSpend;
            projectTaskRepository.Save(task);

            return RedirectToAction("TaskView", "Home", new { taskId = task.Id });
        }

        public IActionResult TaskView(int taskId)
        {
            var task = projectTaskRepository.GetById(taskId);

            return View(task);
        }

        [HttpGet]
        public IActionResult TaskEdit(int taskId)
        {
            ProjectTask task = new ProjectTask();

            if (taskId != 0)
            {
                task = projectTaskRepository.GetById(taskId);
            }

            var users = userRepository.GetAll().ToList();
            ViewBag.Users = users;

            return View(task);
        }

        [HttpPost]
        public IActionResult UserEdit(User user)
        {
            userRepository.Save(user);

            return RedirectToAction("UserView", "Home", new { userId = user.Id });
        }

        public IActionResult TaskDelete(int taskId)
        {
            var task = projectTaskRepository.GetById(taskId);
            projectTaskRepository.Delete(task);

            return RedirectToAction("Tasks", "Home");
        }

        public IActionResult UserDelete(int userId)
        {
            var user = userRepository.GetById(userId);
            userRepository.Delete(user);
            return RedirectToAction("Users", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

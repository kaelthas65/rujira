﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Entities.Enums;

namespace RUJ.Web.Models.AuthModels
{
    public class RegisterModel
    {
        [MaxLength(50)]
        [Required(ErrorMessage = "Email should not be empty!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [MaxLength(20)]
        [Required(ErrorMessage = "Password should not be empty!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [MaxLength(128)]
        [Required(ErrorMessage = "Name should not be empty!")]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        public DepartmentEnum.Department Department { get; set; }

        public RoleEnum.Role Role { get; set; }
    }
}

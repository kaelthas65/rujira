﻿using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.Repositories.ProjectTasks
{
    public class ProjectTaskRepository : IProjectTaskRepository
    {
        private readonly RUJContext context;

        public ProjectTaskRepository(RUJContext context)
        {
            this.context = context;
        }

        public IList<ProjectTask> GetAll()
        {
            return context.ProjectTasks.Include(y => y.User).ToList();
        }

        public IList<ProjectTask> GetByUserId(int userId)
        {
            return context.ProjectTasks.Include(y => y.User).Where(x => x.UserId == userId).ToList();
        }

        public ProjectTask GetById(int id)
        {
            return context.ProjectTasks.Include(y => y.User).First(x => x.Id == id);
        }

        public void Save(ProjectTask projectTask)
        {
            if (projectTask.Id == 0)
                context.ProjectTasks.Add(projectTask);
            else
                context.Entry(projectTask).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(ProjectTask projectTask)
        {
            context.ProjectTasks.Remove(projectTask);
            context.SaveChanges();
        }
    }
}

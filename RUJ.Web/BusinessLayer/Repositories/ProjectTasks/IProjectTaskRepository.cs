﻿using System.Collections.Generic;
using DataLayer.Entities;

namespace BusinessLayer.Repositories.ProjectTasks
{
    public interface IProjectTaskRepository
    {
        IList<ProjectTask> GetAll();

        IList<ProjectTask> GetByUserId(int userId);

        ProjectTask GetById(int id);

        void Save(ProjectTask projectTask);

        void Delete(ProjectTask projectTask);
    }
}

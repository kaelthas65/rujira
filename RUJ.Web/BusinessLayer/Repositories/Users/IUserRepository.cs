﻿using DataLayer.Entities;
using System.Collections.Generic;

namespace BusinessLayer.Repositories.Users
{
    public interface IUserRepository
    {
        IList<User> GetAll();

        User GetById(int id);

        User GetByEmail(string email);

        User GetByName(string fullName);

        User GetByEmailAndPassword(string email, string password);

        void Save(User user);

        void Delete(User user);
    }
}

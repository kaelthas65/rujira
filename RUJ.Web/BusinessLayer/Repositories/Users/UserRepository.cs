﻿using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.Repositories.Users
{
    public class UserRepository : IUserRepository
    {
        private readonly RUJContext context;

        public UserRepository(RUJContext context)
        {
            this.context = context;
        }

        public IList<User> GetAll()
        {
            return context.Users.ToList();
        }

        public User GetById(int id)
        {
            return context.Users.Include(y => y.ProjectTasks).First(x => x.Id == id);
        }

        public User GetByEmail(string email)
        {
            return context.Users.FirstOrDefault(u => u.Email == email);
        }

        public User GetByName(string fullName)
        {
            return context.Users./*Include(x => x.Campaigns).Include(y => y.Comments).*/FirstOrDefault(u => u.FullName == fullName);
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            return context.Users.FirstOrDefault(u => u.Email == email && u.Password == password);
        }

        public void Save(User user)
        {
            if (user.Id == 0)
                context.Users.Add(user);
            else
                context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(User user)
        {
            context.Users.Remove(user);
            context.SaveChanges();
        }
    }
}

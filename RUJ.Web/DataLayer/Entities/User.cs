﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataLayer.Entities.Enums;

namespace DataLayer.Entities
{
    public class User
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(20)]
        public string Password { get; set; }

        [MaxLength(128)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        public DepartmentEnum.Department Department { get; set; }

        public RoleEnum.Role Role { get; set; }

        public List<ProjectTask> ProjectTasks { get; set; }
    }
}

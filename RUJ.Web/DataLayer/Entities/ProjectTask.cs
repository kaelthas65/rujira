﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Entities.Enums;

namespace DataLayer.Entities
{
    public class ProjectTask
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public DepartmentEnum.Department Department { get; set; }

        public int EstimatedTime { get; set; }

        public int SpentTime { get; set; }

        public ProjectTaskStateEnum.ProjectTaskState ProjectTaskState { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }
    }
}

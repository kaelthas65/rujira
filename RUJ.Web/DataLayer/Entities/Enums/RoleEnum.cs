﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities.Enums
{
    public class RoleEnum
    {
        public enum Role
        {
            [Display(Name = "Директор")]
            Director,

            [Display(Name = "Сотрудник")]
            Employee
        }
    }
}

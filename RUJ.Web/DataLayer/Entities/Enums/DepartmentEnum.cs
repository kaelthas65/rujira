﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities.Enums
{
    public class DepartmentEnum
    {
        public enum Department
        {
            [Display(Name = "SEO")]
            SEO,

            [Display(Name = "SMM")]
            SMM,

            [Display(Name = "Контекстная реклама")]
            ContextAdvertisment,

            [Display(Name = "Обучающий центр")]
            Education,

            [Display(Name = "Администрация")]
            Administration
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities.Enums
{
    public class ProjectTaskStateEnum
    {
        public enum ProjectTaskState
        {
            [Display(Name = "Назначена")]
            Assigned,

            [Display(Name = "В работе")]
            InProgress,

            [Display(Name = "Закрыта")]
            Closed
        }
    }
}

﻿using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataLayer
{
    public class RUJContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }

        public RUJContext(DbContextOptions<RUJContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectTask>().HasIndex(a => a.Id);
            modelBuilder.Entity<ProjectTask>().HasIndex(a => a.UserId);
            modelBuilder.Entity<User>().HasIndex(a => a.Id);

            base.OnModelCreating(modelBuilder);
        }

       /* protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server =.; Database = RUJ; Trusted_Connection = True; MultipleActiveResultSets = true", b => b.MigrationsAssembly("DataLayer"));
        }*/
    }

    /*
    public class RUJContextFactory : IDesignTimeDbContextFactory<RUJContext>
    {
        public RUJContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<RUJContext>();
            optionsBuilder.UseSqlServer(@"Server =(localdb)\MSSQLLocalDB; Database = CrowdFunding; Trusted_Connection = True; MultipleActiveResultSets = true", b => b.MigrationsAssembly("DataLayer"));

            return new RUJContext(optionsBuilder.Options);
        }
    }*/

}
